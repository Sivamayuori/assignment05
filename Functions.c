//This is a program to identify the relationship between profit and ticket price
//Logic considered: Increase or decrease of attendance by 20 for Rs.5 
#include <stdio.h>

//Function for revenue calculation
int revenue_cal(int price);
//Function for expenditure calculation
int expenditure_cal(int price);
//Function for profit calculation
int profit_cal (int price);
//Function for attendance calculation
int attendance_cal(int price);

//Display the relationship
void display ();
int attendance_cal (int price){
	const int attendance = 120;

	return attendance - ((price-15)/5*20);
}

int revenue_cal(int price){
	return price * attendance_cal(price);
}

int expenditure_cal(int price){
	const int cost = 500;
	
	return cost + 3*attendance_cal(price);
}

int profit_cal(int price){
	return revenue_cal(price) - expenditure_cal(price);
}

void display(){
	int i=5, profit;
	for (i=5; i<=50; i+=5){
		profit = profit_cal(i);
		printf ("   Rs.%d \t Rs.%d \n",i,profit);
	}
}

int main (){
	//Dispaly the relationship
	printf ("The table displays the relationship between profit and ticket price \n");
	//Dispaly the ticket price and profit
	printf ("Ticket price: \t Profit: \n");
	display ();
	//Dispaly the maximum profit
	printf ("According to the table, profit increases when the ticket price is in the range of Rs.5-Rs.25 and decreases after. \nMaximum profit is Rs.1260 for Rs.25 ticket price.\n");
	return 0;
}

